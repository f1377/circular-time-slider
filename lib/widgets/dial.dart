import 'dart:math';

import 'package:circular_time_slider/widgets/dial_painter.dart';
import 'package:flutter/material.dart';

class Dial extends StatefulWidget {
  final Duration duration;
  final ValueChanged<Duration> onChanged;
  final double snapToMins;

  const Dial({
    Key? key,
    required this.duration,
    required this.onChanged,
    this.snapToMins = 1.0,
  }) : assert(duration != null);

  @override
  _DialState createState() => _DialState();
}

class _DialState extends State<Dial> with SingleTickerProviderStateMixin {
  static const double _kTwoPi = 2 * pi;
  static const double _kPiByTwo = pi / 2;
  static const double _kCircleTop = _kPiByTwo;

  Tween<double>? _thetaTween;
  Animation<double>? _theta;
  AnimationController? _thetaController;
  ThemeData? themeData;
  MaterialLocalizations? localizations;
  MediaQueryData? media;
  Offset? _position;
  Offset? _center;

  double _pct = 0.0;
  int _hours = 0;
  bool _dragging = false;
  int _minutes = 0;
  double _turningAngle = 0.0;

  @override
  void initState() {
    super.initState();
    _thetaController = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
    _thetaTween = Tween<double>(begin: _getThetaForDuration(widget.duration));
    _theta = _thetaTween!.animate(
        CurvedAnimation(parent: _thetaController!, curve: Curves.fastOutSlowIn))
      ..addListener(() => setState(() {}));
    _thetaController!.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _hours = _hourHand(_turningAngle);
        _minutes = _minuteHand(_turningAngle);
        setState(() {});
      }
    });

    _turningAngle = _kPiByTwo - widget.duration.inMinutes / 60.0 * _kTwoPi;
    _hours = _hourHand(_turningAngle);
    _minutes = _minuteHand(_turningAngle);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    assert(debugCheckHasMediaQuery(context));
    themeData = Theme.of(context);
    localizations = MaterialLocalizations.of(context);
    media = MediaQuery.of(context);
  }

  @override
  void dispose() {
    _thetaController!.dispose();
    super.dispose();
  }

  static double _nearest(double target, double a, double b) {
    return ((target - a).abs() < (target - b).abs()) ? a : b;
  }

  void _animateTo(double targetTheta) {
    final double currentTheta = _theta!.value;
    double beginTheta =
        _nearest(targetTheta, currentTheta, currentTheta + _kTwoPi);
    beginTheta = _nearest(targetTheta, beginTheta, currentTheta - _kTwoPi);
    _thetaTween!
      ..begin = beginTheta
      ..end = targetTheta;
    _thetaController!
      ..value = 0.0
      ..forward();
  }

  double _getThetaForDuration(Duration duration) {
    return (_kPiByTwo - (duration.inMinutes % 60) / 60.0 * _kTwoPi) % _kTwoPi;
  }

  Duration _getTimeForTheta(double theta) {
    return _angleToDuration(_turningAngle);
  }

  Duration _notifyOnChangedIfNeeded() {
    _hours = _hourHand(_turningAngle);
    _minutes = _minuteHand(_turningAngle);

    var d = _angleToDuration(_turningAngle);

    widget.onChanged(d);
    return d;
  }

  void _updateThetaForPan() {
    setState(() {
      final Offset offset = _position! - _center!;
      final double angle = (atan2(offset.dx, offset.dy) - _kPiByTwo) % _kTwoPi;

      if (angle >= _kCircleTop &&
          _theta!.value <= _kCircleTop &&
          _theta!.value >= 0.1 &&
          _hours == 0) {
        return;
      }

      _thetaTween!
        ..begin = angle
        ..end = angle;
    });
  }

  void _handlePanStart(DragStartDetails details) {
    assert(!_dragging);
    _dragging = true;
    final RenderBox box = context.findRenderObject() as RenderBox;
    _position = box.globalToLocal(details.globalPosition);
    _center = box.size.center(Offset.zero);

    //_updateThetaForPan();
    _notifyOnChangedIfNeeded();
  }

  void _handlePanUpdate(DragUpdateDetails details) {
    double oldTheta = _theta!.value;
    _position = _position! + details.delta;
    _updateThetaForPan();
    double newTheta = _theta!.value;

//    _updateRotations(oldTheta, newTheta);
    _updateTurningAngle(oldTheta, newTheta);
    _notifyOnChangedIfNeeded();
  }

  int _hourHand(double angle) {
    return _angleToDuration(angle).inHours.toInt();
  }

  int _minuteHand(double angle) {
    // Result is in [0; 59], even if overall time is >= 1 hour
    return (_angleToMinutes(angle) % 60.0).toInt();
  }

  Duration _angleToDuration(double angle) {
    return _minutesToDuration(_angleToMinutes(angle));
  }

  Duration _minutesToDuration(minutes) {
    return Duration(
        hours: (minutes ~/ 60).toInt(), minutes: (minutes % 60.0).toInt());
  }

  double _angleToMinutes(double angle) {
    // Coordinate transformation from mathematical COS to dial COS
    double dialAngle = _kPiByTwo - angle;

    // Turn dial angle into minutes, may go beyond 60 minutes (multiple turns)
    return dialAngle / _kTwoPi * 60.0;
  }

  void _updateTurningAngle(double oldTheta, double newTheta) {
    if (newTheta > 1.5 * pi && oldTheta < 0.5 * pi) {
      _turningAngle = _turningAngle - ((_kTwoPi - newTheta) + oldTheta);
    } else if (newTheta < 0.5 * pi && oldTheta > 1.5 * pi) {
      _turningAngle = _turningAngle + ((_kTwoPi - oldTheta) + newTheta);
    } else {
      _turningAngle = _turningAngle + (newTheta - oldTheta);
    }
  }

  void _handlePanEnd(DragEndDetails details) {
    assert(_dragging);
    _dragging = false;
    _position = null;
    _center = null;
    //_notifyOnChangedIfNeeded();
    _animateTo(_getThetaForDuration(widget.duration));
  }

  void _handleTapUp(TapUpDetails details) {
    final RenderBox box = context.findRenderObject() as RenderBox;
    _position = box.globalToLocal(details.globalPosition);
    _center = box.size.center(Offset.zero);
    _updateThetaForPan();
    _notifyOnChangedIfNeeded();

    _animateTo(_getThetaForDuration(_getTimeForTheta(_theta!.value)));
    _dragging = false;
    _position = null;
    _center = null;
  }

  List<TextPainter> _buildMinutes(TextTheme textTheme) {
    final TextStyle? style = textTheme.subtitle2;

    const List<Duration> _minuteMarkerValues = const <Duration>[
      const Duration(hours: 0, minutes: 0),
      const Duration(hours: 0, minutes: 5),
      const Duration(hours: 0, minutes: 10),
      const Duration(hours: 0, minutes: 15),
      const Duration(hours: 0, minutes: 20),
      const Duration(hours: 0, minutes: 25),
      const Duration(hours: 0, minutes: 30),
      const Duration(hours: 0, minutes: 35),
      const Duration(hours: 0, minutes: 40),
      const Duration(hours: 0, minutes: 45),
      const Duration(hours: 0, minutes: 50),
      const Duration(hours: 0, minutes: 55),
    ];

    final List<TextPainter> labels = <TextPainter>[];
    for (Duration duration in _minuteMarkerValues) {
      var painter = new TextPainter(
        text: new TextSpan(style: style, text: duration.inMinutes.toString()),
        textDirection: TextDirection.ltr,
      )..layout();
      labels.add(painter);
    }
    return labels;
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    int selectedDialValue = 0;
    _hours = _hourHand(_turningAngle);
    _minutes = _minuteHand(_turningAngle);

    Color backgroundColor;
    Color accentColor = themeData!.colorScheme.secondary;
    switch (themeData!.brightness) {
      case Brightness.light:
        backgroundColor = Colors.grey.shade200;
        break;
      case Brightness.dark:
        backgroundColor = themeData!.backgroundColor;
        break;
    }
    print('backgroundColor: $backgroundColor');

    return GestureDetector(
      excludeFromSemantics: true,
      onPanStart: _handlePanStart,
      onPanUpdate: _handlePanUpdate,
      onPanEnd: _handlePanEnd,
      onTapUp: _handleTapUp,
      child: CustomPaint(
        painter: DialPainter(
          pct: _pct,
          multiplier: _hours,
          minuteHand: _minutes,
          context: context,
          selectedValue: selectedDialValue,
          labels: _buildMinutes(theme.textTheme),
          backgroundColor: backgroundColor,
          accentColor: accentColor,
          theta: _theta!.value,
          textDirection: Directionality.of(context),
        ),
      ),
    );
  }
}
