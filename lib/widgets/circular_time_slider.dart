import 'package:flutter/material.dart';

import 'dial.dart';

class CircularTimeSlider extends StatelessWidget {
  static const double _kDurationPickerWidthPortrait = 328.0;
  static const double _kDurationPickerHeightPortrait = 380.0;

  final Duration duration;
  final ValueChanged<Duration> onChanged;
  final double snapToMins;

  final double? width;
  final double? height;

  const CircularTimeSlider({
    Key? key,
    this.duration = const Duration(minutes: 0),
    required this.onChanged,
    required this.snapToMins,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? _kDurationPickerWidthPortrait / 1.5,
      height: height ?? _kDurationPickerHeightPortrait / 1.5,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Dial(
              duration: duration,
              onChanged: onChanged,
              snapToMins: snapToMins,
            ),
          ),
        ],
      ),
    );
  }
}
