import 'package:circular_time_slider/screens/circular_time_slider_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(DurationPickerExample());
}

class DurationPickerExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CircularTimeSliderScreen(),
    );
  }
}
